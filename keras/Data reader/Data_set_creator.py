import numpy as np
import os
from scipy import *
import pandas as pd
#import sklearn
#from sklearn import utils
import pickle
import random
from numpy import random
from math import floor

# function to read data files in a given directory and extract the position timeseries and class labels
def load_data(npoints, data_dir):
    path_files_TD = [f for f in os.listdir(data_dir) if f.endswith('.dat')]  # search forlder for data files
    
    # sort the datasets by order of the particles
    path_files_TD=sorted(path_files_TD)
    
    #store the info for each file
    x_windows = [] #xdata  
    y_windows = [] #ydata
    zsum_windows = [] #intensity data
    
    nsegments_min = []
    for file in range(len(path_files_TD)):
        current_dir = "".join((data_dir,path_files_TD[file]))
        
        current_df = pd.read_csv(current_dir,skiprows=2,header=None,delimiter='\t')
        
        nsegments = int(len(current_df[0])/npoints)
        nsegments_min.append(nsegments)
        
        # [files, segments, timeseries]
        x_windows.append([current_df[0][int(i*npoints):int((i+1)*npoints)] for i in range(nsegments)])
        y_windows.append([current_df[1][int(i*npoints):int((i+1)*npoints)] for i in range(nsegments)])
        zsum_windows.append([current_df[2][int(i*npoints):int((i+1)*npoints)] for i in range(nsegments)])
        
        print(current_dir + " Read "+ str(nsegments) + " segments", end = '\r')

    # free memory
    del current_df
    del nsegments

    #x_windows = np.array(x_windows)
    #y_windows = np.array(y_windows)
    #zsum_windows = np.array(zsum_windows)
    
    # limit number of segments for an unbiased training (some particles with more segments than others)
    nsegments_min = np.min(nsegments_min)
    
    data = np.zeros((3, len(path_files_TD), nsegments_min, npoints))

    # Mean centering of the position points referencing to the mean position value of each segment
    for p in range(len(path_files_TD)):
        for i in range(nsegments_min):
            xmean = np.mean(x_windows[p][i][:])
            ymean = np.mean(y_windows[p][i][:])
            zmean = np.mean(zsum_windows[p][i][:])
            data[0,p,i,:] = x_windows[p][i][:] - xmean
            data[1,p,i,:] = y_windows[p][i][:] - ymean
            data[2,p,i,:] = zsum_windows[p][i][:] - zmean    

    # free memory
    del x_windows
    del y_windows
    del zsum_windows

    targets = np.array([path_files_TD[i].replace(".dat","")[:-1] for i in range(len(path_files_TD))]) #type of target
    print(targets.shape)

    return targets, data

def make_image(xdata, ydata, P, x, y):  # data format [image, position data] and P is the number of bins
    images = np.zeros((len(xdata),P,P))
    xmax, xmin = np.max(xdata), np.min(xdata)
    xmin, xmax = -x,x
    xedges = np.linspace(xmin, xmax, P+1)
    ymax, ymin = np.max(ydata), np.min(ydata)
    ymin, ymax = -y,y
    yedges = np.linspace(ymin, ymax, P+1)
    for p in range(len(xdata)):
        hist, xbins, ybins = np.histogram2d(xdata[p],ydata[p], bins=(xedges, yedges), density=False)
        images[p] = hist
    return images

def normalize2D(X_train): #, X_test):
    # normalize each pixel
    for x in range(X_train.shape[1]):  # normalize each pixel to 0 to 1
        for y in range(X_train.shape[2]):
            for j in range(X_train.shape[0]):
                if np.max(X_train[:,x,y])==0:
                    continue
                X_train[j,x,y] = X_train[j,x,y]/np.max(X_train[:,x,y])
    #for x in range(X_test.shape[1]):
    #    for y in range(X_test.shape[2]):
    #        for j in range(X_test.shape[0]):
    #            if np.max(X_test[:,x,y])==0:
    #                continue
    #            X_test[j,x,y] = X_test[j,x,y]/np.max(X_test[:,x,y])

    # normalize each image
    for i in range(X_train.shape[0]):
        X_train[i] = X_train[i]/np.max(X_train[i])
    #for i in range(X_test.shape[0]):
    #    X_test[i] = X_test[i]/np.max(X_test[i])
        
    return X_train#, X_test

# dictionary type class
class mydatabase:
    def __init__(self, targets, data):
        self.targets = targets
        self.data = data
        self.classes = np.unique(targets, return_counts=False)

    # this function removes n from each class of particles and removes it from the complete set. Returns: complete set without the removed entities, targets of the previous set, validation set, validation targets, test set, test targets
    def random_particle_separator(self, N_validation, N_test):
        data = self.data
        targets = self.targets

        # separation of particles for the validation set
        validation_set = np.zeros((self.data.shape[0], int(N_validation * self.classes.shape[0]), self.data.shape[2], self.data.shape[3], self.data.shape[4])) # [planes, particles, segments, bins x, bins y]
        validation_targets = np.repeat(self.classes, N_validation)  # new set target array creation
        for i in range(self.classes.shape[0]):
            for n in range(N_validation):
                index = np.random.choice(np.where(self.classes[i] == targets)[0],1) # choice of a random particle in a given class
                validation_set[:, i*N_validation + n] = data[:, index]
                # removal of particles from complete dataset
                targets = np.delete(targets, index, 0)
                data = np.delete(data, index, 1)
        
        # separation of particles for the test set
        test_set = np.zeros((self.data.shape[0], int(N_test * self.classes.shape[0]), self.data.shape[2], self.data.shape[3], self.data.shape[4])) # [planes, particles, segments, bins x, bins y]
        test_targets = np.repeat(self.classes, N_test)  # new set target array creation
        for i in range(self.classes.shape[0]):
            for n in range(N_test):
                index = np.random.choice(np.where(self.classes[i] == targets)[0],1) # choice of a random particle in a given class
                test_set[:, i*N_test + n] = data[:, index]
                # removal of particles from complete dataset
                targets = np.delete(targets, index, 0)
                data = np.delete(data, index, 1)

        return data, targets, validation_set, validation_targets, test_set, test_targets

    # creates every possible combination of p elements of the classes
    def leave_p_out(self, p):
        # each line is a class, the elements of the line are the indexes of the class in the targets/data arrays
        index_matrix = np.array([np.where(self.classes[i] == self.targets)[0] for i in range(len(self.classes))])

        def factorial(n):
            return np.prod(np.arange(1,n+1,1))

        n_class_combinations = int(factorial(index_matrix.shape[1])/(factorial(index_matrix.shape[1]-p)*factorial(p)))

        print(">>> Every class has ", n_class_combinations, "possible combinations, resulting in ", n_class_combinations**index_matrix.shape[0], "total combinations!")

        # check if two arrays have the same elements
        def bool_arrays(array1, array2):
            if len(array1)!=len(array2):
                return False
            else:
                array1 = np.sort(array1)
                array2 = np.sort(array2)
                #print(np.array([array1[i]==array2[i] for i in range(len(array1))]).all())
                return np.array([array1[i]==array2[i] for i in range(len(array1))]).all()

        in_class_combinations = np.zeros((index_matrix.shape[0], n_class_combinations, p), dtype="int")
        # matrix of every possible combination of p elements inside the classes
        
        for i in range(index_matrix.shape[0]):
            indexes = np.array([index_matrix[i] for j in range(p)])
            class_combinations = np.array(np.stack(np.meshgrid(*indexes),-1)).reshape(-1, p)  # for each class creates every possible combination of two indices
            count = 0
            #print(class_combinations)
            #print("single", in_class_combinations[i])
            #removal of repeated combinations of indices and sets with same index number
            for k in range(class_combinations.shape[0]):
                #print(np.sort(class_combinations[k]), np.sort(in_class_combinations[i]))
                #print(not np.array([bool_arrays(class_combinations[k], in_class_combinations[i, l]) for l in range(in_class_combinations.shape[1])]).all())

                # if every element of combination is different and not saved yet record the combination               
                if ((len(np.unique(class_combinations[k], return_counts=False)) == len(class_combinations[k])) and (not np.any(np.array([bool_arrays(class_combinations[k], in_class_combinations[i, l]) for l in range(in_class_combinations.shape[1])])))):
                    #print(class_combinations[k])
                    in_class_combinations[i, count] = class_combinations[k].astype(int)
                    count += 1

        #print(indexes)
        #print(indexes.shape)
        #print(indexes[-1])
        #print(*indexes[-1])
        #print(class_combinations)
        #print(class_combinations.shape)
        #print(in_class_combinations)
        #print(in_class_combinations.shape)
        
        # returns every possible combination of p entities from each class 
        return np.array(np.stack(np.meshgrid(*in_class_combinations),-1)).reshape(-1, in_class_combinations.shape[0])
    


    # this code goes through the targets labels and separates the type of particle and size. It is useful if we want to create classes that take into account only size or type.
    def properties(self, keywords):
        def name_filter(name, keywords):
            # removes the micrometer um
            name = name.replace("um","")    #print(name)
            sizes = ""
            names = ""
            for i in name:
                if i.isdigit():
                    sizes += i
            if name == "water":
                sizes += "water"
            #print(name)
            for j in keywords:
                if j in name:
                    names += j
                    continue
            return [names, sizes]

        single_unit = np.array([name_filter(self.targets[i], keywords) for i in range(len(self.targets))])
        properties = {}
        properties["type"] = np.array(single_unit[:,0])
        properties["size"] = np.array(single_unit[:,1])
        return properties

    # function that creates sets of indices for the k-fold cross-validation
    def k_fold(self, k, targets, classes):
        count = np.min(np.unique(targets, return_counts=True)[1])
        print(count)

        # each line is a class, the elements of the line are the indexes of the class in the targets/data arrays
        index_matrix = [np.random.permutation(np.where(classes[i] == targets)[0]) for i in range(len(classes))]
        print(index_matrix)
        #print(index_matrix)
        #print(index_matrix.shape[1])
        #print(k)
        #print(round(index_matrix.shape[1]/k - 0.5))
        
        index_matrix_shape_1 = np.zeros((len(index_matrix)))
        for i in range(len(index_matrix_shape_1)):
            index_matrix_shape_1[i] = len(index_matrix[i])
        index_matrix_shape_1 = int(np.min(index_matrix_shape_1))
        print(index_matrix_shape_1)
        print(type(index_matrix_shape_1))
        for i in range(len(index_matrix)):
            index_matrix[i] = index_matrix[i][:index_matrix_shape_1]
        print(index_matrix)
        index_matrix = np.array(index_matrix)
        print(">>> In total there are ", floor(index_matrix.shape[1]/k), "k-fold combinations for k =", k)

        #print(index_matrix)

        print(index_matrix.shape)
        print(index_matrix.shape[1])
        #combinations = np.array([index_matrix[:][k*sets:k*sets+k] for sets in range(floor(index_matrix.shape[1]/k))]).reshape(floor(index_matrix.shape[1]/k), k*index_matrix.shape[0])
        combinations = np.matrix.transpose(index_matrix)
        #print(combinations)
        #print(combinations.reshape(floor(index_matrix.shape[1]/k), k*index_matrix.shape[1]))

        return combinations

"""
    # function that creates sets of indices for the k-fold cross-validation
    def k_fold(self, k, targets, classes):
        # each line is a class, the elements of the line are the indexes of the class in the targets/data arrays
        index_matrix = np.array([np.random.permutation(np.where(classes[i] == targets)[0]) for i in range(len(classes))], dtype="object")
        print(index_matrix)
        #print(index_matrix)
        #print(index_matrix.shape[1])
        #print(k)
        #print(round(index_matrix.shape[1]/k - 0.5))

        print(">>> In total there are ", floor(index_matrix.shape[1]/k), "k-fold combinations for k =", k)

        #print(index_matrix)

		#combinations = np.zeros((index_matrix.shape[1],index_matrix.shape[0]))
		#for i in range(combinations.shape[0]):
		#    combinations[i] = 

        print(index_matrix.shape)
        print(index_matrix.shape[1])
        combinations = np.array([index_matrix[:][k*sets:k*sets+k] for sets in range(floor(index_matrix.shape[1]/k))]).reshape(floor(index_matrix.shape[1]/k), k*index_matrix.shape[0])
		
        #print(combinations)
        #print(combinations.reshape(floor(index_matrix.shape[1]/k), k*index_matrix.shape[1]))

        return combinations
"""


# this functions performs the pixel by pixel normalization resulting in donut like distributions
def donut_normalize(images):
    for x in range(images.shape[2]):
        for y in range(images.shape[3]):
            if np.max(images[:,:,x,y])==0.:
                images[:,:,x,y] = 0.
                continue
            images[:,:,x,y] = images[:,:,x,y]/np.max(images[:,:,x,y])
    return images

# this funtion normalizes each images maximum to 1
def image_normalizer(images, P, nplanes):
    for particle in range(images.shape[0]):
        for segment in range(images.shape[1]):
            for planes in range(nplanes):
                if np.max(images[particle, segment, 0:P, planes*P: (planes+1)*P]) == 0:
                    images[particle, segment, 0:P, planes*P: (planes+1)*P] = images[particle, segment, 0:P, planes*P: (planes+1)*P]
                    images[particle, segment, P:2*P, planes*P: (planes+1)*P] = images[particle, segment, P:2*P, planes*P: (planes+1)*P]
                    continue
                images[particle, segment, 0:P, planes*P: (planes+1)*P] = images[particle, segment, 0:P, planes*P: (planes+1)*P]/np.max(images[particle, segment, 0:P, planes*P: (planes+1)*P])
                images[particle, segment, P:2*P, planes*P: (planes+1)*P] = images[particle, segment, P:2*P, planes*P: (planes+1)*P]/np.max(images[particle, segment, P:2*P, planes*P: (planes+1)*P])
    return images

def main(classes, npoints = 50000, data_dir="../Data/Experimental data/Synthetic Particles (IAC, 21)/", filename="dataset", P=100, nplanes=1, xlim=0.1, ylim=0.1, xlim_zoom=0.025, ylim_zoom=0.025, load_data=load_data, make_image=make_image, donut_normalize=donut_normalize, image_normalizer=image_normalizer, mydatabase=mydatabase):
    # read and extract position timeseries onto arrays of segments for each particle. The positions are mean centered
    targets, data = load_data(npoints, data_dir)

    # reshape of data to remove the particle selection, this way we have a bunch of segments in a row. data.shape[2] is the number of segments of each particle that are found in a row. In other words, the data.shape[2] first segments belong to one particle and the data.shape[2] following are from another particle...
    #data = np.reshape(data, newshape=(data.shape[0], data.shape[1]*data.shape[2], data.shape[3]))

    # plotting density histograms without normalization (each bin is the counting of times the particle is found in it, this can be relevant if any normalization is done taking into account different directions)
    images = np.zeros((data.shape[1], data.shape[2], 2*P, nplanes*P))
    #images_zoom = np.zeros((nplanes, data.shape[1], data.shape[2], P, P))
    for i in range(nplanes):
        for j in np.arange(i,max(2,nplanes),1):
            if i!=j:
                #print(i,j)
                for particle in range(data.shape[1]):
                    for segment in range(data.shape[2]):
                        images[particle,:, 0:P, (i+j-1)*P:(i+j)*P] = make_image(data[i, particle,:,:], data[j, particle,:,:], P, xlim, ylim)
                        images[particle,:, P:2*P, (i+j-1)*P:(i+j)*P] = make_image(data[i, particle,:,:], data[j, particle,:,:], P, xlim_zoom, ylim_zoom)
     
	#print(images[0,0])
	            
    # normalize images
    images = donut_normalize(images) # donut like normalization
    images = image_normalizer(images, P, nplanes)

    database = mydatabase(targets, images)

    # creates folder "Pickle" if one doesn't exist
    if not (os.path.exists(data_dir+ "Pickle")):
        os.makedirs(data_dir+ "Pickle")
    
    # saves the class "mydatabase" with the images onto a pickle file
    with open(data_dir+ "Pickle/" + filename +'.pickle', 'wb') as f:
        pickle.dump(database, f)

    print(">>> asd Database pickled on " + data_dir + "Pickle/" + " with the name" + filename + "!")




