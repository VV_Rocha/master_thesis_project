import numpy as np
import matplotlib.pyplot
from numba import jit

from numba.pycc import CC
cc = CC('brownian_motion_sim')

import random

@jit(nopython=True)
def init_parameters(delta_x, delta_v, initial_type):  # generation of random initial parameters
    if initial_type==0:
        x0 = np.zeros(6)  # [x, vx, y, vy, z, vz]
        for i in range(0,6,2):
            x0[i] = delta_x*(1.-2.*np.random.rand())
            x0[i+1] = delta_v*(1.-2.*np.random.rand())
        return x0
    else:
        return np.array([delta_x[0], delta_v[0], delta_x[1], delta_v[1], delta_x[2], delta_v[2]])
    
@jit(nopython=True)
def force(r, k, w, naxis, theta = 0.):  # w is the parameter waste
    F = np.zeros(naxis)
    if (naxis == 3):
        x, y, z = r[0], r[1], r[2]
        kx, ky, kz = k[0], k[1], k[2]
        if np.sqrt(x**2 + y**2 + z**2)<=w:  # check is particles are inside the beam, if outside force is zeros
            F[0] = -1.* (kx*(x*np.cos(theta)-y*np.sin(theta))*np.cos(theta)+(ky*(x*np.sin(theta)+y*np.cos(theta))*np.sin(theta)))
            F[1] = -1.* (-1.*kx*(x*np.cos(theta)-y*np.sin(theta))*np.sin(theta)+(ky*(x*np.sin(theta)+y*np.cos(theta))*np.cos(theta)))
            F[2] = -1.*kz*z
    elif (naxis==1):
        x = r[0]
        kx = k[0]
        if x>=w:
            F = np.array([-kx*x])
    return F

@jit(nopython=True)
def func(y, gama, diff, m, k, delta_t, random, w, naxis, force = force):
    r = y[::2]
    v = y[1::2]
    F = force(r, k, w, naxis)
    K = np.zeros(int(2*naxis))
    K[::2] = v
#    K[1::2] = np.array([- gama*v[i]/m + F[i]/m + np.sqrt(2*gama*gama*diff)*(1-2*np.random.rand())/(m*np.sqrt(delta_t)) for i in range(3)])
#    K[1::2] = np.array([- gama*v[i]/m + F[i]/m + np.sqrt(2.*gama*gama*diff)*(np.random.normal(0.,1.))/(m*np.sqrt(delta_t)) for i in range(3)])
    v_storage = np.zeros(naxis)
    for axis in range(naxis):
        v_storage[axis] = - gama*v[axis]/m + F[axis]/m + np.sqrt(2.*gama*gama*diff)*(random[axis])/(m*np.sqrt(delta_t))
    K[1::2] = v_storage
    #K[1::2] = np.array([- gama*v[i]/m + F[i]/m + np.sqrt(2.*gama*gama*diff)*(random[i])/(m*np.sqrt(delta_t)) for i in range(naxis)])
#    K[1::2] = np.array([- gama*v[i]/m + F[i]/m + diff*(np.random.normal(0.,1.))/(m*np.sqrt(delta_t)) for i in range(3)])
    return K

@jit(nopython=True)
def RK4(y, delta_t, gamma, diff, m, k, random, w, naxis, func = func, force = force):  # returns the parameter vector increments 
    K0 = delta_t*func(y, gamma, diff, m, k, delta_t, random[:,0], w, naxis, force)
    K1 = delta_t*func(y + K0/2., gamma, diff, m, k, delta_t/2, random[:,1], w, naxis, force)
    K2 = delta_t*func(y + K1/2., gamma, diff, m, k, delta_t/2, random[:,1], w, naxis, force)
    K3 = delta_t*func(y + K2, gamma, diff, m, k, delta_t/2, random[:,2], w, naxis, force)
    return (K0 + 2.*K1 + 2.*K2 + K3)/6.
    

@jit(nopython=True)
def run(y, T, delta_t, gamma, diff, m, k, w, naxis, RK4 = RK4, func = func, force = force):  # function that does an simulation run by calling the RK4 integrator T times
    rand = np.zeros((naxis,3))
    rand[:,0] = np.array([np.random.normal(0.,1.) for axis in range(naxis)])
    for t in range(1,T):
        #rand[:, 1:] = np.array([np.array([np.random.normal(0.,1.) for k in range(1,3)]) for axis in range(naxis)])
        for axis in range(naxis):
            for s in range(1,3):
                rand[axis, s] = np.random.normal(0.,1.)
        
        y[t] = y[t-1] + RK4(y[t-1], delta_t, gamma, diff, m, k, rand, w, naxis)
        rand[:, 0] = rand[:, 2]
    return y

@jit(nopython=True)
def main(k, gamma, diff, m, N, T, delta_t, w, initial_parameters = np.array([0.,0.,0.,0.,0.,0.]), naxis = 3, run = run, RK4 = RK4, func = func, force = force):
    np.random.seed(4)  # 4, 5, 6
    experiments = np.full((N, T, int(2*naxis)), 0.)  # [x, vx, y, vy, z, vz] storage of all positions and velocities
    for r in range(N):  # loops through the N experiments to be performed
        # defining initial parameters
        experiments[r,0,::] = initial_parameters
        # run
        experiments[r] = run(experiments[r], T, delta_t, gamma, diff, m, k, w, naxis)
    return experiments[:, :, ::2]
    # Return of N experiments by M times matrix
