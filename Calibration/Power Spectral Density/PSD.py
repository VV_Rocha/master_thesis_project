import numpy as np
import scipy as sp
from scipy import signal
from scipy.optimize import curve_fit
from numba import jit

"""
#@jit(nopython=True)
def Lorentzian(f, m, fc, D, gamma):
    return D/(np.pi**2 *((2*np.pi)**2 * m**2 *f**4/gamma**2 + fc**2 - 2*np.pi*m*fc*f/gamma + f**2))
"""
#@jit(nopython=True)
def PSD(data, dt, nperseg):
    freqs, psd = sp.signal.welch(data, 1/dt, nperseg=nperseg, noverlap=int(nperseg/2))
    return freqs, psd

def Lorentzian(f, k, gamma, D, m):
    return gamma**2 * D/(np.pi**2 * ((2*np.pi*m)**2 * (-f**2 + k/((2*np.pi)**2 *m))**2 + gamma**2 *f**2))

def Lorentzian_fp(f, D, fp):
    return D* fp**2 /(np.pi**2 *(f**4 + fp**2 *f**2))

#def Lorentzian(f, k, gamma, D):
#    return gamma**2 * D/(np.pi**2 * ((k/((2*np.pi)))**2 + gamma**2 *f**2))

def Lorentzian(f, D, fc):
    return D / ((np.pi**2)* (fc**2 + f**2))

#@jit()
def main(data, dt, nperseg, naxis, T, maxfev, mean=False, PSD = PSD, Lorentzian_fp=Lorentzian_fp, Lorentzian=Lorentzian):
    psd_storage = np.zeros((data.shape[0], naxis, int(nperseg/2 +1)))
    freqs = np.zeros(int(nperseg/2 +1))
    for axis in range(naxis):
        for exp in range(data.shape[0]):
            freqs, psd_storage[exp, axis, :] = sp.signal.welch(data[exp, :, axis], 1/dt, nperseg=nperseg, noverlap=int(nperseg/2))

#    def Lorentzian(f, k, gamma):
#        return T/(gamma* np.pi**2 *(k**2 /((2*np.pi*gamma)**2) + f**2 ) )

#    def Lorentzian(f, D, fc, fp):
#        return D*fp**2 /(np.pi**2 * (f**4 + (fp**2 - 2*fc*fp)*f**2 + fp**2 *fc**2))

    if mean==True:
        psd_mean = np.zeros((psd_storage.shape[2], naxis))
        psd_var = np.zeros((psd_storage.shape[2], naxis))
        p = np.zeros((2, naxis))
        p2 = np.zeros((2, naxis))
        p_cov = np.zeros((2, naxis))
        fp = np.zeros((2,naxis))
        fc = np.zeros((2,naxis))
        for axis in range(naxis):
            for f in range(int(nperseg/2 +1)):
                psd_mean[f, axis] = np.mean(psd_storage[:, axis, f])
            
            """fp[:, axis] = sp.optimize.curve_fit(Lorentzian_fp, freqs[-int(99*len(freqs)/100):], psd_mean[-int(99*len(freqs)/100):, axis], bounds=([0., 0.],[np.inf, np.inf]))[0]  # Here we estimate the value of fp
            print("fp estimate =", fp[1,axis])
            def Lorentzian_fc(f, D, fc):
                return D/(np.pi**2 * (f**4 /fp[1,axis]**2 + f**2 - 2*fc*f**2 /fp[1,axis]))
            fc[:, axis] = sp.optimize.curve_fit(Lorentzian_fc, freqs[1::], psd_mean[:, axis][1::], bounds=([0., 0.],[np.inf, np.inf]))[0]
            print(sp.optimize.curve_fit(Lorentzian_fc, freqs[1::], psd_mean[:, axis][1::], bounds=([0., 0.],[np.inf, np.inf]))[0])

            #fc[1, :] = 0.015
            print("fc estimate =", fc[1,axis])
            def Lorentzian_m(f, k, gamma, D):
                return gamma**2 * D/(np.pi**2 * ((2*np.pi)**2 * (k/((2*np.pi)**2))**2 + gamma**2 *f**2))

            p[0:3, axis], cov = sp.optimize.curve_fit(Lorentzian_m, freqs[freqs<fp[1,axis]], psd_mean[freqs<fp[1,axis],axis], bounds=([0., 0., 0.],[np.inf, np.inf, np.inf]))
            p_cov[0:3,axis] = np.array([cov[i,i] for i in range(3)])
            p2, cov = sp.optimize.curve_fit(Lorentzian, freqs[freqs>=fp[1,axis]/10.], psd_mean[freqs>=fp[1,axis]/10., axis], bounds=([0., 0., 0., 0.],[np.inf, np.inf, np.inf, np.inf]))
            p[3, axis] = p2[3]
            p_cov[3,axis] = cov[3,3]"""
        
            p[:, axis] = sp.optimize.curve_fit(Lorentzian, freqs, psd_mean[:, axis], bounds=([0., 0.],[np.inf, np.inf]))[0]
            print(p[:, axis])

#            p2[:,axis], cov = sp.optimize.curve_fit(Lorentzian_m, freqs[freqs>=lim_freq], psd_mean[freqs>=lim_freq, axis], bounds=([0., 0., 0.],[np.inf, np.inf, np.inf]))
#            p[0,axis] = p2[0,axis]
#            p_cov[0, axis] = cov[0,0]

#            def Lorentzian2(f, gamma, m):
#                return p[0, axis] * gamma**2 /(np.pi**2 * (2*np.pi)**2 *m**2 *(f**4 + ((gamma/(2*np.pi*m))**2 - 2*p[1,axis]*gamma/(2*np.pi*m))*f**2 + p[1,axis]**2 *(gamma/(2*np.pi*m))**2))

#            p[2:4,axis], cov = sp.optimize.curve_fit(Lorentzian2, freqs, 1./ psd_mean[:, axis], bounds=([0., 0.],[np.inf, np.inf]), maxfev=100000)
#            p_cov[2:4,axis] = np.array([cov[i,i] for i in range(2)])

        psd_storage = psd_mean
    else:
        print(">>> Determination of expected values is only ready for mean=True!")
        p=0.
        p_cov=0.
        
    return psd_storage, freqs, p, p_cov, fp





##################################################
##################################################
##################################################

