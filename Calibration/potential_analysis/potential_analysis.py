import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from scipy.optimize import curve_fit
from numba import generated_jit, jit
from scipy.fft import fft, fftshift

from numba.pycc import CC
cc = CC('brownian_motion_sim')

@jit(nopython=True)
def counting(P, T, pos, bin):  # counts the frequency in each bin window
    frequency = np.zeros(P)
    running_index = 0  # variable to avoid cycles counting the same values
    for i in range(1,P):
        bin_frequency = 0
        while pos[running_index] >= bin[i-1] and pos[running_index] <= bin[i] and running_index < T-1:
            bin_frequency += 1
            running_index += 1
        frequency[i] = bin_frequency  # later alter this to avoid creating the "bin_frequency" variable
    return frequency/(np.sum(frequency)*(bin[1] - bin[0]))

@jit(nopython=True)
def exp_mean(data):
    mean = np.full((data.shape[1],3),0.)
    for b in range(data.shape[1]):
        for axis in range(3):
            mean[b, axis] = np.mean(data[::, b, axis])
    return mean

#@jit(nopython=True)
#@generated_jit(nopython=True)
def main(data, P, gama, diff, w, counting = counting, exp_mean = exp_mean):
    bin_edges = np.full((P+1, 3), 0.)  # [bins, axis]
    """for axis in range(3):
        bin_edges[:, axis] = np.linspace(np.min(data[:, :, axis]), np.max(data[:, :, axis]), P+1)  # [bin, axis]
    dx, dy, dz = bin_edges[1,0]- bin_edges[0,0], bin_edges[1,1]-bin_edges[0,1], bin_edges[1,2]-bin_edges[0,2]"""

    print(">>> Counting:")
    density = np.full((data.shape[0], P, 3), 0.)  # [experiment, bin, axis] for storing the frequencies for each experiment and axis
    for exp in range(data.shape[0]):
        for axis in range(3):
            #density[exp, :, axis], bin_edges[:,axis] = np.histogram(data[exp, :, axis], bins=bin_edges[:, axis], density = True)
            density[exp, :, axis], bin_edges[:,axis] = np.histogram(data[exp, :, axis], bins=P, range=(-w,w), density = True)
    print(">>> Done!")

    print(">>> Determining mean density:")
    mean_density = exp_mean(density)  # [bin, axis], returns the mean of each bin between experiments
    print(">>> Done!")

    # variance
    if data.shape[0]>=2:
        print(">>> Determining density variances:")
        var = np.full((P, 3), 0.)
        for axis in range(3):
            for b in range(P):
                var[b, axis] += np.var(density[:, b, axis], ddof=1.)
        var = var/(data.shape[0] - 1.)
        print(">>> Done!")
    else:
        print(">>> Variance cannot be determined with less than one experiment performed.")
        var = np.full((P, 3), 0.)
    
    @jit(nopython=True)
    def f(x, k, x_eq, offset):
        return np.sqrt(k/(2.*np.pi*gama*diff)) * np.exp(-k * (x - x_eq)**2. / (2.*gama*diff)) + offset

    #f = lambda x, k, x_eq, offset: np.sqrt(k/(2.*np.pi*gama*diff)) * np.exp(-k * (x - x_eq)**2. / (2.*gama*diff)) + offset  # defining function to adjust curve_fit to
    # weighted non-linear regression
    par_est = np.zeros((3,3))  # [estimate, axis]
    par_var = np.zeros((3,3))  # [variance, axis]
    
    bin_center = ((bin_edges[:-1,:]+bin_edges[1:,:])/2)

    for axis in range(3):
        par_est[:,axis], var_matrix = sp.optimize.curve_fit(f, bin_center[mean_density[:,axis]>0., axis], mean_density[mean_density[:,axis]>0.,axis], sigma=np.sqrt(var[mean_density[:,axis]>0.,axis]), absolute_sigma=False, maxfev=10**4, bounds=((0., -np.inf, -np.inf), (np.inf, np.inf, np.inf)))
        par_var[:, axis] = np.array([var_matrix[i,i] for i in range(3)])
        
    return mean_density, var, bin_center, par_est, par_var





##############################################################
##############################################################
##############################################################
