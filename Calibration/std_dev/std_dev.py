import numpy as np

def main(data, gama, diff):
    var = np.full((data.shape[0], 3), 0.)
    mean = np.full((data.shape[0], 3), 0.)
    for exp in range(data.shape[0]):
        for axis in range(3):
            var[exp, axis] = np.var(data[exp, :, axis], ddof=0.)
            mean[exp, axis] = np.mean(data[exp,:,axis])
    x_eq = np.zeros(3)
    for axis in range(3):
        x_eq[axis] = np.mean(mean[:,axis])
    k_estimate = np.zeros(3)
    k_var = np.zeros(3)
    for axis in range(3):
        k_estimate[axis] = np.mean(gama*diff/var[:,axis])
        k_var[axis] = np.var(gama*diff/var[:,axis], ddof=1.)

    return k_estimate, k_var, x_eq
    